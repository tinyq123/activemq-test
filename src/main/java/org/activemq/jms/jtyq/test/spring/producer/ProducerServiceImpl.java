package org.activemq.jms.jtyq.test.spring.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.jms.*;

/**
 * @author TYQ
 * @create 2018-07-21 15:10
 * @desc
 **/
public class ProducerServiceImpl implements ProducerService {

    @Autowired
    private JmsTemplate jmsTemplate;
    //防止多个注入无效
    @Resource(name = "queueDestination")
    private Destination destination;

    @Override
    public void sendMessage(String message) {
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage textMsg = session.createTextMessage("Hello World");
                System.out.println("创建消息: "+textMsg);
                return textMsg;
            }
        });
        System.out.println("发送消息: "+message);
    }
}
