package org.activemq.jms.jtyq.test.spring.producer;

/**
 * @author TYQ
 * @create 2018-07-21 14:57
 * @desc
 **/
public interface ProducerService {

    void sendMessage(String message);

}
