package org.activemq.jms.jtyq.test.spring.producer;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author TYQ
 * @create 2018-07-21 22:03
 * @desc
 **/
public class AppProducer {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:producer-spring.xml");
        ProducerService service = context.getBean(ProducerService.class);
        for (int i = 0;i<100;i++){
            service.sendMessage("text"+i);
        }
        System.exit(0);
    }
}
