package org.activemq.jms.jtyq.test.spring.consumer;

import com.sun.org.apache.xpath.internal.SourceTree;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * @author TYQ
 * @create 2018-07-21 22:22
 * @desc
 **/
public class ConsumerMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        TextMessage textMsg = (TextMessage) message;
        try {
            System.out.println(textMsg.getText());
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
