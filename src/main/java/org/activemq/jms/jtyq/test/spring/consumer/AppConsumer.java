package org.activemq.jms.jtyq.test.spring.consumer;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author TYQ
 * @create 2018-07-21 22:13
 * @desc
 **/
public class AppConsumer {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:consumer-spring.xml");
    }
}
