package org.activemq.jms.jtyq.test;


/**
 * @author TYQ
 * @create 2018-07-21 14:18
 * @desc 消息生产者
 **/

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Scanner;

public class AppProducer {

    public static final String  url = "tcp://localhost:61616";
    public static final String queueName = "queue-test";

    public static void main( String[] args ) throws JMSException {
        //1.创建ConnectionFactory
        ConnectionFactory factory = new ActiveMQConnectionFactory(url);
        //2.创建连接
        Connection connection = factory.createConnection();
        //3.启动连接
        connection.start();
        //4.创建会话
        Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
        //5.创建目标
        Destination destination = session.createQueue(queueName);
        //6.创建一个生产者
        MessageProducer producer = session.createProducer(destination);

        Scanner scanner = new Scanner(System.in);

        while(true){
            String line = scanner.nextLine();
            if("quit".equals(line)){
                break;
            }
            TextMessage textMessage = session.createTextMessage(line+"-"+System.currentTimeMillis());
            producer.send(textMessage);
        }
        connection.close();



    }
}
