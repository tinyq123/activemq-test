package org.activemq.jms.jtyq.test;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * @author TYQ
 * @create 2018-07-21 14:18
 * @desc 消息消费者
 **/
public class AppConsumer {

    public static final String  url = "tcp://localhost:61616";
    public static final String queueName = "queue-test"; // 队列模式，
    public static final String tpicName = "topic-test";  // 主题模式

    public static void main( String[] args ) throws JMSException {
        //1.创建ConnectionFactory
        ConnectionFactory factory = new ActiveMQConnectionFactory(url);
        //2.创建连接
        Connection connection = factory.createConnection();
        //3.启动连接
        connection.start();
        //4.创建会话
        Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
        //5.创建目标
        Destination destination = session.createQueue(queueName);

        //Destination destination =  session.createTopic(topicName) 如果是使用主题模式

        //6.创建一个消费者
        MessageConsumer consumer = session.createConsumer(destination);
        //7.创建一个监听器
        consumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                TextMessage textMessage = (TextMessage) message;
                try {
                    System.out.println("接收消息-"+System.currentTimeMillis()+"："+textMessage.getText());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
