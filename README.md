Apache ActiveMQ 学习简单项目 
-- 
## 一、 下载 ActiveMQ

> ActiveMQ先要启动消息服务器，消息服务器负责如果接收生产者生产的消息，转发给消费者消息

```
cd $activemq-root-dir/bin 
./activemq start 启动服务
./activemq stop 关闭服务
```
> 使用浏览器访问 http://localhost:8186 可以进入ActiveMQ 消息管理页面 
> 默认的账户和密码是 admin/admin

  
1. 消息是由生产者producer发出，由consumer消费。
2. 消费者由MessageListener监听并且处理 。
3. 主题模式，队列模式等。
4. 与Spring集成
   
 